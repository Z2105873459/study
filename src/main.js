import Vue from 'vue'
import App from './App.vue'
import request from './request/index'
import router from './router/index'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false;

// Vue.prototype. 他做的事情是，在
Vue.prototype.$axios = request;
Vue.use(ElementUI);
// document.cookie="avtives_Token="+'325307fa223a4ab08fdaf0413a64a34e'; 
localStorage.avtives_Token = '13f3b106d95e40bc8618962c374fba7f'
//根实例 render 渲染
new Vue({
  render: h => h(App),
  router,//全局嵌入
}).$mount('#app')
