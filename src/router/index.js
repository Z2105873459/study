import Vue from 'vue';
import Router from 'vue-router';


Vue.use(Router);//在一个新的Vue   
import content1 from '../components/content1.vue';
import content2 from '../components/content2.vue';
import element from '../components/element.vue';
// import headerA from './components/header.vue'
// import footerB from './components/footer.vue'
// import HelloWorld from '../components/HelloWorld.vue'
let routes = [
  {
    path:'/',
    component:content1
  },
  {
    path:'/content2',
    component:content2
  },
  {
    path:'/element',
    component:element
  }
];
//当路由改变的时候 
export default new Router({
  mode:"history",
  routes
})