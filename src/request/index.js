import axios from 'axios'


// axios 返回的实例 返回给 requests，然后我们就可以通过requests去调用axios的方法
const requests = axios.create({
  baseURL: 'https://manage.test.rantech.cn', // 基础url,如果是多环境配置这样写，也可以像下面一行的写死。
  // baseURL: 'http://168.192.0.123',
  timeout: 6000 // 请求超时时间
})

//请求前的配置
requests.interceptors.request.use(config => {
  //   const token = Vue.ls.get(ACCESS_TOKEN)
  //看本地存储有没有token
  const token = localStorage.getItem('avtives_Token')
  const data = config.data;
  config.data = {};
  config.data.options = data;
  config.data.common = {
          "appVersion": "",
          "deviceGroupId": "",
          "userId": "",
          "projectId": "hn",
          "device": {
              "udid": "",
              "deviceId": "",
              "mac": ""
          },
          "channelId": "hn",
          token
  }
  console.log(config,'config-zzzz');
  return config
})

//请求 就是数据返回的了，先怎么处理
requests.interceptors.response.use((response) => {
  const res = response.data;
  console.log(res,'res');
  if (res.common.reset != 21000) {
    return Promise.reject(res.common.desc)
  } else {
    return res
  }
})

export default requests;