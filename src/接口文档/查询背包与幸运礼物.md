#### 接口名称：

查询参数

#### 接口地址：
`/api/manage/data/queryGift`
#### 请求方法
`POST`
#### 请求参数：
```
{
    "common": {
        "appVersion": "",
        "deviceGroupId": "",
        "userId": "",
        "projectId": "hn",
        "device": {
            "udid": "",
            "deviceId": "",
            "mac": ""
        },
        "channelId": "hn",
        "token": "c4c97f58f98346739b89903a0c696ea2"//管理员令牌，登录接口中有返回这个
    },
    "options": {
    	"name": "三" // 礼物名称
	}
}
```
#### 返回值：
```
{
    "common": {
        "reset": "21000",
        "desc": "成功",
        "status": "0"
    },
    "options": {
        "items": [
            {
                "name": "三生三世",// 礼物名称
                "giftType": "backGift",// 礼物类型，backGift：背包礼物，luckGift：幸运礼物
                "price": 334400,// 礼物价格
                "propId": "101_334400_6"// 礼物id
            },
            {
                "name": "三生三世",// 礼物名称
                "giftType": "luckGift",// 礼物类型，backGift：背包礼物，luckGift：幸运礼物
                "price": 334400,// 礼物价格
                "propId": "1_334400_2"// 礼物id
            }
        ]
    }
}
```
 
